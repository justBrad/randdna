#include <string>
#include <random>

using std::string;

string randDNA(int seed, string bases, int n)
{
	std::mt19937 eng1(seed);
	string sequencedDNA = "";
	std::uniform_int_distribution<> unifrm(0, bases.size()-1);
	for (int theLength=0; theLength<n; theLength++){
		sequencedDNA += bases[unifrm(eng1)];
	}
	return sequencedDNA;
}